package com.example.everything.Fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.everything.ProductsAdapter
import com.example.everything.ProductsApi
import com.example.everything.R
import com.example.everything.Records
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_product.*


class ProductFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_product, container, false)
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        CallApi()
    }
    fun CallApi(){
        val ApiService= ProductsApi.create()
        ApiService.getProduct()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result->
                Log.i("result",result.toString())
                renderCounrty(result.records)
            },{
                    error->
                error.printStackTrace()
            })
    }

    private fun renderCounrty(productsList: List<Records>){
        recyclerView.layoutManager= LinearLayoutManager(activity)
        recyclerView.adapter= ProductsAdapter(productsList)
    }

}