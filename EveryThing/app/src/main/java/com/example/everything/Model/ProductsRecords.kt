package com.example.everything

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ProductModels(
    @Expose
    @SerializedName("products")
    var records:List<Records>
)

 data class Records(

     @SerializedName("id")
     @Expose
     var scooterId:String,

    @SerializedName("name")
    @Expose
    var scooterName: String? = null,

    @SerializedName("price")
    @Expose
    var scooterPrice: String? = null,

    @SerializedName("mileage")
    @Expose
    var scooterMileage: String? = null,

    @SerializedName("engine_power")
    @Expose
    var scooterEnginepower: String? = null,
    @SerializedName("unit_power")
    @Expose
    var scooterUnitpower: String? = null,

    @SerializedName("gravity")
    @Expose
    var scooterGravity: String? = null,

    @SerializedName("image")
    @Expose
    var image: String? = null
)