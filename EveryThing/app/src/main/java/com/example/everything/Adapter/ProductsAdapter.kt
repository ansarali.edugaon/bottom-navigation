package com.example.everything

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.productlist_item.view.*




class ProductsAdapter( val products:List<Records>):RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
       return ProductViewHolder(
           LayoutInflater.from(parent.context)
               .inflate(R.layout.productlist_item,parent,false)
       )
    }

    override fun getItemCount() = products.size
    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {

        val product= products[position]

        holder.view.productScooter_textView.text = product.scooterName
        holder.view.price_textView.text = product.scooterPrice
        holder.view.scooter_mileage_textView.text = product.scooterMileage
        holder.view.scooter_enginePower_textView.text = product.scooterEnginepower
        holder.view.scooter_powerUnit_textView.text = product.scooterUnitpower
        holder.view.scooter_gravity_textView.text = product.scooterGravity

        Glide.with(holder.view.context)
            .load(product.image)
            .into(holder.view.scooter_imageView)
    }

    class ProductViewHolder(val view: View):RecyclerView.ViewHolder(view)
}
