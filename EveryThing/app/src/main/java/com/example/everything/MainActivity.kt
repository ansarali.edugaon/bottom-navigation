package com.example.everything

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ListView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.everything.Fragments.HomeFragment
import com.example.everything.Fragments.ProductFragment
import com.example.everything.Fragments.ReferFragment
import com.example.everything.Fragments.ServiceFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadFragment(HomeFragment())

        navigation_bar.setOnNavigationItemSelectedListener { menuItem ->
            when{

                menuItem.itemId == R.id.product_nav ->{
                    loadFragment(ProductFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                menuItem.itemId == R.id.home_nav ->{
                    loadFragment(HomeFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                menuItem.itemId == R.id.refer_nav ->{
                    loadFragment(ReferFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                menuItem.itemId == R.id.service_nav ->{
                    loadFragment(ServiceFragment())
                    return@setOnNavigationItemSelectedListener true
                }

                else ->{
                    return@setOnNavigationItemSelectedListener false
                }

            }
        }

    }

    private fun loadFragment(fragment:Fragment){
        supportFragmentManager.beginTransaction().also { fragmentTransaction ->
            fragmentTransaction.replace(R.id.frame_layout,fragment)
            fragmentTransaction.commit()
        }
    }

}