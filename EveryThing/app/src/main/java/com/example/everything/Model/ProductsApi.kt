package com.example.everything

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ProductsApi {

    @GET("products")
    fun getProduct(): Observable<ProductModels>
    companion object Factory{
        fun create():ProductsApi{
            val retrofit= Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("https://demo4346101.mockable.io/")
                .build()
            return (retrofit.create(ProductsApi::class.java))
        }
    }
}